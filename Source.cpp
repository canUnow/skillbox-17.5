#include<iostream>

using namespace std;

class Vector
{
private:
	int x;
	int y = 3;
	int z = 4;
public:
	void SetX(int valueX)
	{
		x = valueX;
	}

	void Print()
	{
		cout << "X = " << x << "\t Y = " << y << "\t Z = " << z << endl << endl;
	}


};

int main()
{
	Vector a;
	a.SetX(7);
	a.Print();

	return 0;
}